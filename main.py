import machine
import neopixel
import urequests
import time
import ntptime
import os
#import mws2

np = neopixel.NeoPixel(machine.Pin(4), 50)
np.write()

cat_lut = {
	'VFR': (0, 255, 0),
	'MVFR': (0, 0, 255),
	'IFR': (255, 0, 0),
	'LIFR': (128, 0, 64)
}
brightness = 1
led_cats = []


def get_color(cat):
	color = cat_lut[cat] if cat in cat_lut else (0, 0, 0)
	return tuple(int(x * brightness) for x in color)


def get_categories():
	with open('airports.txt') as f:
		airports = f.readlines()
	airports = [x.strip() for x in airports]
	led_cat = [''] * len(airports)

	field_names = "station_id,flight_category,sky_cover,cloud_base_ft_agl,visibility_statute_mi,observation_time"
	lat = [28, 30.5]
	lon = [-84, -80]
	#url = "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=csv&mostRecentForEachStation=constraint&hoursBeforeNow=24&fields=" + field_names + "&stationString="
	#url += ','.join((x for x in airports if x))
	url = "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=csv&mostRecentForEachStation=constraint&hoursBeforeNow=24&fields=" + field_names + "&minLat=%.1f&maxLat=%.1f&minLon=%.1f&maxLon=%.1f" % (lat[0], lat[1], lon[0], lon[1])
	content = urequests.get(url).text.strip()
	lines = tuple(x for x in content.split('\n') if x)

	for metar in lines[6:]:
		fields = tuple(metar.split(','))
		station_id = fields[1]
		observation_time = fields[2]
		visibility = fields[10]
		sky = fields[15]
		bases = fields[16]
		category = fields[23]
		#print('station=%s time=%s vis=%s sky=%s bases=%s cat=%s' % (station_id, observation_time, visibility, sky, bases, category))

		try:
			offset = airports.index(station_id)
			print('Updated station ID ' + station_id + ' to ' + category)
		except ValueError:
			print('Skipping station ID ' + station_id)
		else:
			led_cat[offset] = category

	return led_cat


def calc_leds():
	for i in range(np.n):
		if i < len(led_cats):
			np[i] = get_color(led_cats[i])


def update_brightness():
	try:
		with open('set_brightness') as f:
			brightness_str = f.read()
		global brightness
		brightness = float(brightness_str) / 255
	except OSError:
		return False
	except ValueError:
		try:
			os.remove('set_brightness')
		except OSError:
			pass
		print('ERROR: Unable to parse {:s} into a number.'.format(brightness_str))
		return False
	else:
		#try:
		#	os.remove('set_brightness')
		#except OSError:
		#	pass
		return True


try:
	ntptime.settime()
except:
	pass
rtc = machine.RTC()

update_at = time.time()

while True:
	while update_at > time.time():
		if update_brightness():
			calc_leds()
			np.write()

		if 'force_update' in os.listdir():
			break

		time.sleep(0.1)

	led_cats = get_categories()
	calc_leds()
	np.write()

	try:
		os.remove('force_update')
	except OSError:
		pass

	print('Updated at %d' % time.time())
	#print('Updated at {0:04d}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}'.format(*rtc.now()))
	update_at += 5 * 60

