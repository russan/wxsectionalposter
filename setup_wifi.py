import network
import wifi_sta_cfg
import time
from machine import Pin

ap_if = network.WLAN(network.AP_IF)
ap_if.active(True)
ap_if.config(essid='JAX Sectional', password='sectional')

sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.connect(wifi_sta_cfg.SSID, wifi_sta_cfg.PASS)

p2 = Pin(2, Pin.OUT, value=1)
timeout_at = time.time() + 30
while not sta_if.isconnected() and time.time() < timeout_at:
	p2.off()
	time.sleep(0.1)
	p2.on()
	time.sleep(0.4)

if sta_if.isconnected():
	ip = sta_if.ifconfig()
	print('Connected to SSID "' + wifi_sta_cfg.SSID + '" IP ' + ip[0])
	p2.on()
else:
	print('Unable to connect to SSID "' + wifi_sta_cfg.SSID + '"')
	p2.off()

